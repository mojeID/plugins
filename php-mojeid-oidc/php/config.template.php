<?php
/* Copy this file to 'config.local.php' and set the appropriate values. */

define('OPEN_ID_PROVIDER_URL', 'https://mojeid.regtest.nic.cz');
define('OPEN_ID_CLIENT_ID', 'ClientIDHere');
define('OPEN_ID_CLIENT_SECRET', 'ClientSecretHere');

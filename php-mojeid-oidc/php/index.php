<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.local.php'; // do not forget to create it using config.template.php

use Jumbojett\OpenIDConnectClient;

$oidc = new OpenIDConnectClient(OPEN_ID_PROVIDER_URL, OPEN_ID_CLIENT_ID, OPEN_ID_CLIENT_SECRET);
$oidc->setAllowImplicitFlow(false);
$oidc->addScope(['profile']);
$oidc->authenticate();
$name = $oidc->requestUserInfo('given_name');
?>

<html>
<head>
    <title>Sample OpenID Connect Client for MojeID</title>
</head>
<body>
    <p>
        Hello <?php echo $name; ?>!
        You are authenticated using <?php echo OPEN_ID_PROVIDER_URL; ?>.
    </p>
</body>
</html>

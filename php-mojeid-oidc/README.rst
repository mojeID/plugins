Ukázka přihlášení k MojeID pomocí PHP klienta
=============================================

Požadavky
---------

Co je třeba mít nainstalováno před pokračováním:

-  `Composer <https://getcomposer.org/>`__
-  `Docker Engine <https://docs.docker.com/engine/install/>`__

Instalace
---------

1. Ve složce tohoto příkladu spustíme následující příkazy:

   .. code-block:: sh

         cd php
         composer install

         # vytvoření konfiguračního souboru pro konkrétní službu
         cp config.{template,local}.php

         # spuštění webového serveru
         sudo docker compose -f ../docker/docker-compose.yml up

2. Provedeme `ruční registraci klienta
   MojeID <https://www.mojeid.cz/dokumentace/html/ImplementacePodporyMojeid/OpenidConnect/Registrace/index.html>`__.

   -  Do *seznamu URI* je třeba vyplnit URI, přes kterou Váš webový
      prohlížeč přistupuje k PHP aplikaci (složce ``php`` z tohoto
      příkladu). Při použití přiloženého dockerového řešení na vlastním
      počítači lze zadat ``https://localhost:8443/``.

      -  Adresu, se kterou webový server pracuje, můžeme zjistit
         z metody ``OpenIDConnectClient::getRedirectURL()``.
      -  Pokud neodpovídá tomu, co potřebujeme, nastavíme správnou
         adresu metodou ``OpenIDConnectClient::setRedirectURL()``.

3. V souboru ``config.local.php`` vyplníme požadované údaje:

   -  ``OPEN_ID_PROVIDER_URL`` je základní URL služby, ke které se
      chcete připojit
   -  ``OPEN_ID_CLIENT_ID`` je *ID klienta* ze stránky
      https://mojeid.regtest.nic.cz/consumer_admin/
   -  ``OPEN_ID_CLIENT_SECRET`` je *Tajemství klienta* ze stránky
      s podrobnostmi dané služby

      -  na výše uvedené stránce přejdeme v příslušném řádku na odkaz
         *Aktualizovat*

Použití
-------

1. Navštívíme webovou stránku příkladu (https://localhost:8443/).
2. Po případném potvrzení certifikátu s vlastním podpisem budeme
   přesměrováni na přihlašovací stránku MojeID.
3. Po prvním přihlášení budeme vyzváni k souhlasu s předáním údajů.
4. Po potvrzení budeme přesměrováni zpět na stránku naší aplikace, kde
   uvidíme křestní jméno zadaného uživatele
   (pokud jsme udělili příslušný souhlas).
